## MenschensKinder Teltow
Das kommunale Unternehmen "MenschensKinder Teltow" wurde im Jahr 2000 gegründet. Es betreibt elf städtische Kindertagesstätten, sechs Eltern-Kind-Gruppen und ein Familienzentrum. Das Unternehmen hat 1700 betreute Kinder und beschäftigt derzeit 8 Verwaltungsangestellte, 9 Führungskräfte und insgesamt etwa 200 Mitarbeiter.

### Hintergrund
Die Berechnung der Beiträge für die Kinderbetreuung wird regelmäßig in Abhängigkeit vom Einkommen der Eltern und den Vorschriften (Stadt, Kreis und Land) angepasst. Die bis Anfang 2017 verwendete Verwaltungssoftware verwendete zur Berechnung vorgegebene Tabellen. Als die Regelung 2016 auf eine Formel umgestellt wurde, stellte sich heraus, dass mit der bisherigen Software keine formelbasierte Berechnung möglich war.
 
### Warum Tryton?
Wir haben verschiedene neue Softwarelösungen evaluiert. Eine davon war die auf Tryton basierende Demolösung unseres langjährigen IT-Dienstleisters Martin Data Services. Diese Lösung bot alles, was wir uns wünschten, ist gut erweiterbar und sehr fair im Preis.

### Umfang
Die Implementierung umfasst die Registrierung der Familien, die Berechnung des Betreuungskostenanteils, die Zuordnung zur Betreuungseinrichtung, den monatlichen Einzug der Gelder per Lastschrift, nachträgliche Korrekturen des Betrages, Berichte für das Finanzamt, eine Wiedervorlagefunktion, Statistiken, die Verwaltung von Gruppen und Schulklassen, wöchentliche Kurse und die Erstellung von Kontaktlisten für die Betreuer.
 
### Ergebnis
Die Umstellung auf die neue Lösung mit Tryton verlief reibungslos. Die Erfassung der Familien ist einfacher geworden. Der Arbeitsablauf ist jetzt genau so, wie wir ihn brauchen, und die Berichte sind viel einfacher zu erstellen und anzupassen. Die Mitarbeiter, die mit dem Tool arbeiten, sind sehr zufrieden damit!

### Pläne für die nächsten Jahre
Im Moment sind wir dabei, die Zeiterfassung der Mitarbeiter (Anwesenheit) und die Personalplanung in Tryton zu integrieren. Die nächste Aufgabe wird ein Modul für eine Diät-/Menüplanung sein.

### Kontakt
* Kunde: [Kindertagesstätte "MenschensKinder Teltow"](https://mekiteltow.de)
* Kontakt: [Solveig Haller](mailto:s.haller@mekiteltow.de)
* Betreuer: [Martin Data Services Teltow](https://m-ds.de)

## Buchkontor Teltow
Das Buchkontor Teltow ist eine kleine Buchhandlung in Teltow nahe Berlin. Die preisgekrönte Buchhandlung (Schönste Buchhandlung Deutschlands [2018); Deutscher Buchhandlungspreis (2021) bietet in Verbindung mit einem eigenen Verlag Klassiker, Schulbücher, aktuelle Bestseller, Kinderbücher und regionale Literatur. 

### Die Aufgabe
Nach einer Gesetzesänderung mussten Kassensysteme mit TSE ("Technische Sicherheitseinrichtung") ausgestattet werden. TSE ist eine technische Lösung, um Verkäufe zu zertifizieren, zu archivieren und nachträgliche Änderungen zu verhindern. Sie soll die verifizierte Dokumentation von Barverkäufen für die Finanzbehörden sicherstellen, um nicht gemeldete Einnahmen zu unterbinden. Für eine kleine unabhängige Buchhandlung war eine integrierte TSE/POS-Lösung und Buchhaltungssoftware ideal, mit den Komponenten:

* Buchhaltung
* TSE/POS
* Lagerverwaltung
* Schnittstelle zum Buchgroßhändler "G. Umbreit GmbH & Co. KG" 
* Kundenstammverwaltung, inkl. vCard-Kompatibilität
* Druckschnittstelle zu einem Standard-Bondrucker der Fa. Epson

Weder für POS noch TSE existierten fertige Lösungen für Python. Es musste ein Tryton-Kassenmodul entwickelt werden. Wir mussten auch die Buchpreisbindung nach deutschem Recht beachten, das bedeutet, dass wir mit Bruttopreisen kalkulieren müssen. Da es sich beim Buchkontor Teltow um ein kleines Unternehmen handelt, ist die Buchhaltung nicht auf die Bilanzierung, sondern auf die Einnahmenüberschussrechnung (EÜR) angewiesen.

### Die Lösung
Wir haben uns für Tryton 6.0 entschieden, um die Vorteile des LTS (Long Term Support) zu nutzen. Um das Kassensystem dauerhaft verfügbar zu haben, baten wir die Fa. B2CK aus Liége, Belgien um Unterstützung. B2CK ist das Unternehmen der Tryton-Kern-Entwickler, es  hat inzwischen die Arbeiten am Sale-Point-Modul abgeschlossen, das nun der gesamten Tryton-Community und der Öffentlichkeit zur Verfügung steht.

Nachdem wir die verschiedenen Ansätze für TSE geprüft hatten, verwarfen wir eine lokale Hardware-Lösung. Nach mehreren Gesprächen mit Jan Grasnick, der sich ebenfalls mit dem Thema TSE beschäftigt hatte, bündelten wir unsere Bemühungen und entschieden uns für die Cloud-Lösung von der Fa. fiscali. Gemeinsam haben wir eine stabile Fiscali-Cloud-Python-API aufgebaut. 

Der letzte Schritt war die Anbindung an den Großhändler "G. Umbreit GmbH & Co. KG", um alle benötigten Daten (Preise, Bestände und Metadaten) abzurufen. Außerdem haben wir eine Importroutine für die benötigten Daten aus dem bestehenden System erstellt.

### Die Implementierung
Die Implementierung verlief wie geplant. Nach einer Woche Schulung und Tests konnten wir in den Produktívmodus wechseln, importierten die erforderlichen Daten und schalteten TSE vom Test- in den Live-Modus.

### Ergebnisse
Die Mitarbeiter und der Inhaber der Buchhandlung sind sehr zufrieden mit dem neuen System. Es läuft stabil und reibungslos. Durch das flexible Berichtswesen und weitere Anpassungen ist die Buchhandlung nun in der Lage, ihre Steuerberichte selbst zu erstellen und ist nicht mehr auf einen Steuerberater angewiesen. So profitiert die Buchhandlung neben POS und TSE von vielen Funktionen von Tryton 6.0, definitiv ein Erfolg für alle Seiten. 

### Danksagung
Wir bedanken uns bei Cedric Krier, der Tryton Fondation und ihren Unterstützern und der Community!

### Kontakt
* Kunde: [Buchkontor Teltow](https://buchkontor-teltow.de)
* Kontakt: [Vanessa Arend-Martin](mailto:service@buchkontor-teltow.de)
* Betreuer: [Martin Data Services Teltow](https://m-ds.de)
  [Jan Grasnick](https://mittelwind.de): TSE-Anbieter und Mitentwickler
  [Cedric Krier](https://b2ck.com): Sale_Point_Module

## Inmedio Berlin
Inmedio Berlin ist ein Institut, das in den Bereichen 

* Supervision
* Mediation
* Organisationsentwicklung
* Konfliktmanagement
* Friedensmediation und Dialog
* Prävention und Intervention bei sexueller Gewalt
* Krisenintervention

aktiv ist.

Inmedio hat nicht nur in Berlin und Deutschland, sondern auch international einen ausgezeichneten Ruf. Kunden sind Einzelpersonen, Familien, Unternehmen, Universitäten und nationale (z.B. Außenministerium) oder internationale Institutionen. Das Institut verfügt über langjährige Erfahrung in der Moderation internationaler Konflikte in Europa (z.B. Ukraine/Russland, Armenien/Aserbaidschan), Afrika (Äthopien) und Asien (Sri Lanka). Das Kernteam besteht aus fünf Partnern, die hauptsächlich auf Distanz zusammenarbeiten. 

### Die Herausforderung
Die Verwaltung und das Management des Unternehmens stieß mit der Zeit an ihre Grenzen,  Excel, Word, vCard und DropBox waren nicht mehr ausreichend. Die Herausforderung bestand vor allem darin, die weit verstreuten Daten, die in vielen verschiedenen Dateiformaten gespeichert waren, in einem integrierten System zu sammeln. Aufgaben waren:

* Aufbau einer zentralen Instanz für Adressen/Kontakte, kategorisiert und für E-Mail, Telefonsystem und Mobilfunk verfügbar, als Kerninhalte für ein CRM/Customer Relation Management
* Buchhaltung über die Nettoeinkommensmethode
* Verwaltung von Kostenstellen für die Partner
* Verwaltung von Klassen/Gruppen/Klassenmitgliedern, inkl. Angebote, Verträge, Abrechnung, Zeugnisdruck
* Mahnwesen
* aufgrund des hohen Anteils an internationalen Abrechnungen ist eine korrekte Handhabung der deutschen/schweizerischen/europäischen Umsatzsteuerbestimmungen zwingend erforderlich
* eine Cloud-Lösung ist unabdingbar, da viele Arbeiten im Home Office oder auf Reisen erledigt werden
* um die EU-DSGVO einzuhalten, benötigen wir 2-Faktor-Authentifizierung und/oder VPN
* Bankberichte müssen importierbar sein
* in Zukunft: Projekte und Budgetierung

### Die Lösung
Tryton erfüllt die oben genannten Anforderungen perfekt, kombiniert mit einem sehr fairen Total Cost of Ownership. 

Bis auf die vCard-Schnittstelle, die die Kontaktdaten regelmäßig per Cronjob synchronisiert, basiert die Lösung größtenteils auf bestehenden Kernmodulen. 
Unsere Hauptaufgabe bestand darin, die Daten zu sammeln, zu harmonisieren und zu importieren. Eine wichtige Aufgabe war die Erstellung von Dienstleistungsvorlagen als Standard, um eine große Anzahl von speziellen Mehrwertsteuer- und Kostenstellenvariationen abzudecken. Außerdem mussten wir das Berichtswesen verbessern, insbesondere in Bezug auf Kostenstellen und Steuern.

### Die Implementierung
Um das System zum Laufen zu bringen, führten wir Live-Schulungen mit dem Kunden durch und vereinbarten eine wöchentliche Überwachung durch Online-Meetings. Es mussten umfangreiche Kontakt- und Bankinformationen in das System eingegeben werden, aber der positive Einfluss auf die Arbeitsbelastung war bereits nach 6 Wochen spürbar. Das Einrichten der Kostenstellen funktionierte auf Anhieb. Seit 2022 werden alle Rechnungen über das System abgewickelt. 

### Ergebnisse
Nach zwei Monaten Testphase sieht der Steuerbericht so schön aus, wie ein Steuerbericht nur aussehen kann. Der Kunde spart eine Menge Geld, da er keinen externen Steuerberater mehr in Anspruch nehmen muss. 

### Danksagung
Wir bedanken uns bei David Harper (totp-Modul), Korbinian Preisler (Beratung bei der Steuerkonfiguration und viele Tipps!), der Tryton Fondation und deren Unterstützern und Community!

### Anbieter
* Kunde: [Inmedio Berlin](https://https://inmedio.de/de)
* Kontakt: [Inmedio](mailto:berlin@inmedio.de)
* Betreuer: [Martin Data Services Teltow](https://m-ds.de)

## KomponentenKontor Berlin GmbH 
Aufgrund von "Corona" im Frühjahr 2020 brach das Kerngschäft der KomponentenKontor Berlin GmbH, der Austausch von Kofferrädern praktisch völlig zusammen. Es blieb nichts als die Kosten zu senken, und weil die Zusammenarbeit mit dem damals beauftragte Buchhaltungsbüro immer strapaziöser wurde, bot es sich an, die Buchhaltung ins eigene Haus zu holen - und gleich ein professionelles Warenwirtschaftssystem einzuführen.

Mit Tryton haben wir dies mit minimalen Kosten erreicht, und als Abfallprodukt entstand ein nicht unerheblicher Teil des Tryton-Buchs. Heute kostet die Buchhaltung etwa 1/20 des damaligen Betrages, gleichzeitig macht sie nicht wesentliche mehr Arbeit. Als der zeitaufwändigste Teil der Buchhaltung haben sich die Vorarbeiten (Belege zusammensammeln und sortieren) herausgestellt, diese müssen bei Fremdvergabe ohnehin durchgeführt werden.

Der Einstieg in die Tryton-Gemeinschaft hat uns interessante Kontakte und neue Freundschaften beschert und ist menschlich und geschäftlich eine große Bereicherung. 

### Kontakt
* Kunde: [Netzwerk Rollentausch](https://rollentausch.eu)
* Kontakt: [Wolf Drechsel](mailto:wd@trolink.de)
* Betreuer: selbst, mit Unterstützung der Gemeinschaft
