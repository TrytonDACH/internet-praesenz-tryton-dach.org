## Funktionen

Schon in seiner Standardkonfiguration kann Tryton den allergrößten Teil der Aufgaben, die in einem Unternehmen EDV-gestützt erledigt werden können, abdecken, zum Beispiel:

### Kunden- und Lieferantenverwaltung

Natürlich können einem Geschäftspartner mehrere Adressen und Ansprechpartner beliebig zugeordnet werden. 

## Artikelverwaltung

Diese kann mit Warenwirtschaft und Lagerverwaltung genutzt werden, die Warenwirtschaft ist voll in die Buchhaltung integriert. Möglich sind

+ Bestandsüberwachung
+ Einkaufs- und Verkaufsverwaltung
+ Sammelrechnungen
+ Teillieferungen
+ Inventur
+ Bedarfsprognosen
+ Versand- und Lieferkonditionen (Incoterms)

## Buchhaltung

+ Standardkostenrahmen SKR03 und SKR04 – andere Kontenrahmen können nach Belieben eingerichtet werden
+ Bilanzierung mit Einnahme-Überschuss-Rechnung EÜR
+ Umgang mit Fremdwährungen
+ Rechnungslegung
+ Einlesen von Kontoauszügen
+ Mahnwesen
+ Steuerauswertung, Vorbereitung für Steuererklärung (Umsatzsteuer mit Voranmeldung, Bilanz, Gewinn- und Verlustrechnung GuV)
+ offene Posten
+ Kontenblätter/ -auswertung
+ Saldenbestätigung 
+ Kostenstellen, Kostenstellenauswertung
+ Anlagenverwaltung mit Abschreibungsautomatik
+ Budgetierung
+ Rabatte
+ Versand, Anbindung an Versanddienstleister (DPD, UPS, GLS...)


## Projektverwaltung und -Abrechnung

Diese dient zur Abrechnung von Dienstleistungen

## Weitere Funktionen 

Weitere Funktionen können mittels spezialisierter Module abgedeckt werden, zum Beispiel:

+ Kasse (POS) mit Waage, Kassenlade, Drucker, Anzeige etc.
+ Zeiterfassung: Arbeitszeiterfassung mit Urlaubsregelungen
+ ... hier sind der Phantasie keine Grenzen gesetzt.

