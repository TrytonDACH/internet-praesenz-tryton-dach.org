Kitalux ist eine Komplettlösung zur Administration von Kindertagesstätten, Kinderhorten und Kindergärten auf Basis von Tryton ERP. Es wird entwickelt und vertrieben von [Martin Data-Services](https://www.tryton-dach.org/menschen-hinter-tryton-dach#mds) aus Teltow bei Berlin.

Kitalux ist seit 2016 in den kommunalen Kitas der Städte Teltow und Cottbus erfolgreich im Einsatz. Mit den dort gewonnenen Erfahrungen wird es ständig weiterentwickelt.

## Kitalux bietet u.a:

* Erfassung von Kindern nach Familienstruktur (Familie, Eltern, Kinder)
* viele Parameter pro Kind möglich (Notfallkontakte, E-Mail-Kontakte, Kursbelegungen, Allergien, Abholzeiten, u.v.a.)
* Betreuungseinrichtungen/Kindertagesstätten (Krippe, KiGa, Hort), Tagespflege
* Festlegen von Beiträgen nach wählbaren Parametern per Tabelle oder individueller Berechnungsformel
* Essengelder
* Kundenkonto pro Kind
* Abrechnung pro Familie (nur eine Lastschrift für alle Kinder der Familie)
* Lastschriften, Selbstzahler
* SEPA-Lastschrift-XML-Daten zum Einreichen bei der Bank
* Festlegen von Beiträgen pro Kind auch für Änderungen in der Zukunft
* Schulen + Schulklassen, wöchentliche Kurse
* Listen für Kontaktpersonen und Abholberechtigte
* Statistik, flexible Exportabfragen
* Wiedervorlage(n)
* Serienbrief
* Lokal/als Client-Server-Modell oder als Cloud-Lösung
  * Applikationsserver mit PostgreSQL-Datenbank
  * Clientprogramme für Windows, Apple und Linux und per Webbrowser
* [Zeiterfassung für Mitarbeiter](http://www.m-ds.de/2018/04/23/tryton-modul-zeiterfassung-anwesenheitserfassung/), Arbeitszeitmodelle

Kitalux verfügt inzwischen über eine Vielzahl von Modulen, die seinen Funktionsumfang noch erweitern können. 
