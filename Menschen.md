## Redaktionsteam von tryton-dach.org

* Korbinian Preisler — [virtual things](https://www.virtual-things.biz)  
  Tryton-Enthusiast, Tryton Foundation Board Member

* Udo Spallek — [virtual things](https://www.virtual-things.biz)  
  Tryton-Evangelist

* Frederik Jaeckel — [martin - data services](https://m-ds.de) — [frederik.jaeckel@m-ds.de]  (mailto:frederik.jaeckel@m-ds.de)
  Tryton-Entwickler seit langen Jahren

* Richard Martin — [martin - data services](https://m-ds.de) — [richard.martin@m-ds.de]  (mailto:richard.martin@m-ds.de)
  Redakteur des Tryton-Buchs

* Jan Grasnick — [jan@mittelwind.de](mailto:jan@mittelwind.de) — [https://mittelwind.de](https://mittelwind.de)
  Tryton-Entwickler seit langen Jahren

* Wolf Drechsel — [KomponentenKontor Berlin GmbH] — [wd@trolink.de](mailto:wd@trolink.de)  
  Redakteur des Tryton-Buchs

* Hartmut Goebel  
  Überzeugt SoLaWis und Genossenschaften von Tryton


## Weitere Tryton-Unterstützer aus der DACH-Region

* Mathias Behrle — [https://www.m9s.biz](https://www.m9s.biz) — [info@m9s.biz](mailto:info@m9s.biz)  
  Maintained die Tryton-Pakete für Debian


## Kommerzielle Dienstleister

Kommerzielle Dienstleister für Tryton finden Sie auf [tryton.org](https://www.tryton.org/service-providers).
