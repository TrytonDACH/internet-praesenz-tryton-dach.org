# Tryton DACH

Dies ist eine Präsenz deutschsprachiger Nutzer und Entwickler des ERP-Systems Tryton.

Was ist TRYTON ?

## Tryton ist Unternehmenssoftware

Ein ERP-(Enterprise Resource Planning) oder ERM-(Enterprise Resource Management)-System ist eine Unternehmenssoftware, die wesentlichen administrativen Unternehmensprozesse EDV-gestützt abbildet. Kernaspekte dabei sind Buchführung und Warenwirtschaft. 

Tryton ist eines der ganz wenigen ERP-Systeme, das vollständig lizenzgebührenfrei in OpenSource angeboten wird. Eine Alternative wäre ERPNext.

Warum empfehlen wir den Einsatz von TRYTON ?

## Tryton ist extrem flexibel

Tryton ist ein Server-Client-System. So kann es in den verschiedensten Umgebungen eingesetzt werden und jederzeit ohne große Probleme migriert werden. 

Hier eine kleine Auswahl möglicher Szenarien – der Fantasie sind keine Grenzen gesetzt:

+ Installation auf einem handelsüblichen Desktop-PC unter Linux, Mac OS oder Windows - Server und Client befinden sich auf dem gleichen Rechner
+ Installation auf eigenem Server - Server und (mehrere) Client(s) befinden sich in einem lokalen Netz — mit Python-Virtual-Environment oder in Docker
+ Cloud-Installation auf einem fremden Server — mit Python-Virtual-Environment oder in Docker
+ Nutzung der Software als SaaS – "Software as a Service" — ein Dienstleister kümmert sich um Installation, Konfiguration und Wartung des Systems.
+ Auftragsvergabe von Buchhaltung und anderen Diensten an einen Dienstleister, der Tryton einsetzt.

Der größte Teil der Informationen, die Tryton nutzt, wird in einer PostgreSQL-Datenbank gespeichert. Diese ist in jedem Szenario gleich, Sie können ohne großen Aufwand zwischen den Installationsarten migrieren.

Selbstverständlich ist Tryton mehrplatzfähig und verfügt über ein differenziertes Rechtemanagement.

## Tryton ist kommunikativ

Durch seine offene Konzeption kann Tryton mit praktisch aller anderer Software „sprechen“, so zum Beispiel

+ Banken (Kontoauszüge, SEPA-Lastschriften)
+ DATEV
+ E-Mail
+ LDAP-Verzeichnisdienste und MS Active Directory 
+ Webshops wie zum Beispiel woocommerce, vue.js, shopify
+ CMS-Systeme
+ myebilanz, ebilanzonline
+ Kassensysteme (POS) mit Waage, Kassenlade, Drucker, Anzeige etc.
+ Zeiterfassungssysteme
+ Fertigungseinrichtungen

Berichte können in vielen Formaten erzeugt werden, beispielsweise als Text- oder Tabellen-Dokumente, als Präsentationen und natürlich als HTML und XML.

Über REST/JSON-API, XMLRPC-API und eine Python-API können beliebige eigene Schnittstellen programmiert werden und natürlich können Daten als Tabellen  importiert und exportiert werden.

## Tryton ist OpenSource

Für den Anwender bedeutet OpenSource das vor allem größtmögliche Flexibilität. Ein Gründer, Kleinst- oder Kleinunternehmer kann von Anfang an eine höchst leistungsfähige Software nutzen, ohne von den Lizenzgebühren erschlagen zu werden. Wächst das Unternehmen, kann sich die Software mitentwickeln, spezielle Anforderungen können jederzeit durch maßgeschneiderte Module erfüllt werden. Entwickler können dabei hunderte vorhandene Module als Vorbild, Inspirationsquelle oder Baustein nutzen - ebenfalls in der Regel lizenzgebührenfrei.

## Tryton ist zukunftssicher

Hinter Tryton steht kein einzelner Programmierer und kein Unternehmen, sondern es wird von einer internationalen Entwicklergemeinschaft gepflegt und weiterentwickelt. Daher ist ausgeschlossen, dass das Projekt Tryton wegen Konkurs das Unternehmens, Aufkauf oder Wechsel in der Unternehmensstrategie stirbt. Die Entwickler verdienen ihr Geld mit der Beratung und dem Entwickeln von Spezialkomponenten für Tryton-Nutzer. Tryton ist also kein reines Ehrenamt-Projekt, wo stets ein gewisses Risiko besteht, dass die Motivation der Projektgründer irgendwann verbraucht ist. Vielmehr hat eine ganze Anzahl von Menschen ein vitales Interesse daran, dass Tryton weiterlebt.

## Tryton spricht Deutsch und 18 andere Sprachen

Tryton ist multilingual, multiwährungsfähig, und lokalisiert. 

Das bedeutet:
Die Benutzeroberfläche kann auf eine Vielzahl von Sprachen eingestellt, alle geschäftlichen Transaktionen können in allen Währungen der Welt administriert werden. Und Tryton erzeugt insbesondere Rechnungen, Lieferscheine und andere Geschäftskorrespondenz in der Landessprache des Geschäftspartners.

## Tryton ist dokumentiert

Unser Tryton-Buch ist eine umfangreiche Anwender-Dokumentation zu Tryton. Es begleitet Sie bei der Installation, der Durchführen von Standard-Abläufen bis zum Jahresabschluss, der Anpassung der Software und bei vielem mehr.
