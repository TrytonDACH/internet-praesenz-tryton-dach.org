Tryton eignet sich bestens zum Betrieb einer Ladenkasse mit TSE. Das Kürzel "TSE" steht für "technische Sicherungseinrichtung", diese ist - mit wenigen Ausnahmen - spätestens zum 31.12.2022 in Deutschland verpflichtend.

Da Tryton mit offenen Standards arbeitet, kann eine Ladenkasse mit preiswerten, auch mit gebrauchten Standard-EDV-Komponenten aufgebaut werden. Ein Warenwirtschafts- und ein Buchhaltungssystem wird dabei quasi mitgeliefert und kann mehr oder weniger ausgebaut und genutzt werden.

Benötigt werden

+ ein handelsüblicher Rechner, es genügt bereits ein Raspberry Pi4,
+ eine handelsübliche Kassenlade mit USB-Anschluß
+ eine handelsüblicher Bildschirm
+ eine handelsübliche EDV-Tastatur

Auf Kundenwunsch kann das System um weitere Komponenten ergänzt werden, zum Beispiel:

+ Preisanzeigebildschirm
+ Waage mit USB-Anschluß
+ Barcode-Scanner mit USB-Anschluß

Natürlich ist eine Mehrbenutzer- und Mehrplatzlösung realisierbar.

Die "technische Sicherungseinrichtung" sorgt dafür, dass alle Transaktionsdaten verschlüsselt auf einem Speicher abgelegt werden. Sie kann auf zwei Arten realisiert werden:

+ als online-Lösung - dazu ist eine ständige Netzverbindung erforderlich; die Kosten liegen (ohne die Netzverbindung) bei ca. 5-15€ pro Monat,
+ mit einem USB-Dongle - diese ist derzeit noch in Entwicklung und wohl ab 2023 verfügbar. Die Kosten dürften bei einigen zig Euro pro Jahr liegen, Details sind noch nicht bekannt.

Die Gesamtkosten eines TSE-Systems auf Tryton-Basis können - je nach vorhandener Hardware, Ausbaugrad des Systems und Eigenleistungsanteil - inklusive Hintergrundsystem bei wenigen hundert Euro liegen. Eine Tryton-basierte TSE-Kasse dürfte damit eine der preiswertesten und gleichzeitig leistungsfähigsten und flexibelsten Lösungen am Markt darstellen.

## Fördermöglichkeiten

Zur Einführung von Digitalisierungslösungen existieren zahlreiche staatliche Fördermöglichkeiten, zum Beispiel unter den Namen "Digitalbonus", "Digitalprämie" etc. Sprechen Sie uns an, wir beraten und helfen bei der Erstellung von Förderanträgen !
