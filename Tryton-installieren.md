Es gibt zahllose Möglichkeiten, Tryton zu installieren. Die Planung komplexer Szenarien ist etwas für Spezialisten, bitte fragen Sie uns.

Für den bei Kleinunternehmen häufigsten Fall - die Einrichtung auf einem Einzelplatzrechner - haben einen klaren Favoriten: Die Installation in einer virtuellen Umgebung mit Python-Virtual-Environmnent. Wir haben diese stark vereinfacht, dazu stellen wir Installationsskripte und Muster-Datenbanken zur Verfügung, die mit den in Deutschland am häufigsten benötigten Einträgen vorkonfiguriert sind.

Details zur Installation und den Optionen finden sich im Tryton-Buch unter Installation.
