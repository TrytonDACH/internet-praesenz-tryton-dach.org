# ZeitLux - Arbeitszeiterfassung / Anwesenheitszeiterfassung

Tryton kann als Basis für das Arbeitszeiterfassungssystem bzw. Anwesenheitszeiterfassungssystem "ZeitLux" von [Martin Data Service](https://m-ds.de) fungieren. 

Mit Zeitlux können sie 

+ beliebig viele Tarif-/Arbeitszeitmodelle verwalten
+ beliebig viele Zeitkonten flexibel konfigurieren
+ Zeiten erfassen (über das Türgerät mit Karte oder Transponder)
+ Zeiten nachtragen (direkt oder per Antrag/Freigabe)
+ Zeiten planen (Dienstplan, Personaleinsatzplanung)
+ Zeiten exportieren (Export wird Ihren Wünschen/Erfordernissen angepasst)
+ Berichte (insbesondere Monatsberichte, Mitarbeiterauswertungen) und Statistiken
+ verschiedene Arbeitszeit- und Tarifmodelle anpassen/einpflegen
+ Überstunden, Mehrstunden, o.ä. auszahlen
+ Urlaub beantragen/freigeben
+ Urlaubsplanung
+ Monatskalender für Urlaub, Ferien und Arbeitszeiten
+ Arbeitspläne erstellen
+ individuell konfigurierbare Nutzerrechte
+ verschiedene optionale Erweiterungen: Export-Schnittstelle zur Lohnbuchhaltung; Koppelung an unsere Kita-Management Software KitaLux; Anbindung an Active Directory u.a.

## Das Türgerät

Das Türgerät mißt HxBxT ca. 12cm x 22cm x 4cm, es ist mit einem 7-Zoll-Touch-Display (800 x 480 Pixel), einem RFID-Reader (13,56 MHz/Mifare-Protokoll) und einem Summer (für Tonsignale bei positiver RFID-Erkennung) ausgestattet. Standardmäßig wird das Gerät per USB-Netzteil (mini-USB-Stecker 5V/3A) mit Strom versorgt, dieses ist Teil des Lieferumfangs. Die Datenübertragung erfolgt per WLAN / WiFi, das Türgerät speichert Eingaben bei WLAN-Ausfall und aktualisiert den Datenbestand bei Wiederaufnahme der Datenverbindung selbsttätig.

Das Türgerät aktiviert sich bei Berührung des Touch-Screen oder mittels RFID-Chip. Nach Aktivierung bleibt das Display einige Sekunden aktiv (konfigurierbar) und schaltet dann in den Ruhemodus (dunkel), alternativ kann auch ein “Info-Modus” aktiviert werden, um z.B. tagesaktuelle Informationen für Passanten (beispielsweise Mitarbeiter oder Kita-Eltern) anzuzeigen.

Wir fertigen das Gerät aus handelsüblichen Standard-Komponenten selbst in Deutschland, es basiert auf den Miniatur-Rechner "RaspberryPi". So sind wir flexibel bei Defekten oder dem Wunsch nach Aufrüstung. Derzeit bieten wir an:

+ Türgerät Standard: mit RaspberryPi Zero W - Stromverbrauch ca. 0,8 W
+ Türgerät Multimedia, auch geeignet zum Abspielen von Videos oder Animationen: mit RaspberryPi 3 - Stromverbrauch ca. 2,4W.

Die Türgeräte können in 8 Farben gefertigt werden, weitere Varianten sind möglich - bitte sprechen Sie uns an. Weitere Details finden Sie unter [](https://zeitlux.de).
