## Kernmodule

Die zentrale Download-Instanz von Tryton findet sich unter

[https://downloads.tryton.org](https://downloads.tryton.org)

Hier sind die Kernmodule sowie die Clients für Linux, MacOS und Windows verfügbar.

## PyPi-Module

[https://pypi.org](https://pypi.org) ist die zentrale Instanz für Python-Module aller Art, hier ist auch eine große Zahl an Tryton-Modulen zu finden. Zu nennen ist hier insbesondere der Kontenrahmen [SKR04](https://pypi.org/project/mds-account-de-skr04).

## DACH-Module

Die deutschsprachige Entwicklergemeinschaft stellt unter

[https://gitlab.com/TrytonDACH](https://gitlab.com/TrytonDACH)

ein Repositorium zur Verfügung, wo

+ eine Sammlung von Skripten zur Installation und der Verwaltung einer Installation,
+ Vorlagen für die "Berichte", also zum Beispiel Rechnungen, Angebote, Packlisten, Lieferscheine etc.,
+ frei verfügbare Module der deutschsprachigen Tryton-Entwicklergemeinschaft 

und andere Software verfügbar sind. Unter dem blauen Feld "clone" rechts außen stehen die Download-Adressen, die Software kann mit dem Befehl

`git clone [Adresse]`

heruntergeladen werden.
